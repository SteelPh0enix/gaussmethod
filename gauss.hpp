#pragma once

#include <vector>
#include <cstdint>
#include <algorithm>

#include "equation.hpp"

enum class GaussMethod : uint8_t {
  SIMPLE = 0,        // 0b00
  SELECT_ROW = 1,    // 0b01
  SELECT_COLUMN = 2, // 0b10
  SELECT_FULL = 3    // 0b11
};


template <typename T>
std::vector<std::pair<char, T>> solve_equations_with_gauss(Equations<T> equations,
                                          GaussMethod method = GaussMethod::SELECT_FULL) {
  
  auto solve_equation = [&]() {
      // Simple method: eliminate variables from top to bottom,
      // and substitute calculated values from bottom to top
      for (unsigned diagonal = 0; diagonal <= equations.A.size(); diagonal++) {
        for (unsigned y = diagonal + 1; y < equations.A.size(); y++) {
          if (equations.substract_multiplied_rows(diagonal, y, diagonal) == false) {
            return std::vector<std::pair<char, T>>{};
          }
        }
      }

      std::cout << "Gorna macierz trojkatna:\n" << equations << '\n';

      // Substitution:
      // We are going to calculate values of variables on matrix diagonal.
      // In order to do that, we'll make identity matrix which will yield 
      // solution of the equation system.
      for (int diagonal = equations.A.size() - 1; diagonal >= 0; diagonal--) {

        // Eliminate every variable on the right of diagonal
        for(unsigned i = 1; i + diagonal < equations.A[diagonal].size(); i++) {
          // Substract single-value row from the row we're calculating
          equations.substract_multiplied_rows(diagonal + i, diagonal, diagonal + i);
        }

        // Make the row an unit row.
        equations.B[diagonal] /= equations.A[diagonal][diagonal];
        equations.A[diagonal][diagonal] = 1;
      }

      std::cout << "Rozwiazana macierz jednostkowa:\n" << equations << '\n';
      
      // Now the problem may be that variables in matrix are not in the same order
      // as it's values. That's where `matrix_variables` vector comes helpful.
      std::vector<std::pair<char, T>> results;

      for(unsigned x = 0; x < equations.B.size(); x++) {
        results.push_back({equations.matrix_variables[x], equations.B[x]});
      }
      
      // ...and then sort it by variable name.
      std::sort(results.begin(), results.end(), 
                [](std::pair<char, T> const& a, std::pair<char, T> const& b) {
                  return a.first < b.first;
                });
        
      return results;
  };

  switch(method) {
    case GaussMethod::SIMPLE: {
      std::cout << "Rozwiazywanie metoda prosta.\n";

      // Nothing to be done here. Lambda above (called at return) will do the work.
      break;
    }
    case GaussMethod::SELECT_ROW: {
      std::cout << "Rozwiazywanie metoda wyboru wiersza.\n";
      // Selecting the row with biggest value on diagonal column and swapping it.
      for(unsigned diagonal = 0; diagonal < equations.A.size(); diagonal++) {
        equations.swap_rows(diagonal, equations.find_max_in_column(diagonal, diagonal));
      }

      std::cout << "Uporzadkowane rownanie:\n" << equations << '\n';

      // At this point, we should have correct matrix, solvable with simple method.
      break;
    }
    case GaussMethod::SELECT_COLUMN: {
      std::cout << "Rozwiazywanie metoda wyboru kolumny.\n";
      // Selecting the column with biggest value on diagonal row and swapping it.
      for(unsigned diagonal = 0; diagonal < equations.A.size(); diagonal++) {
        equations.swap_columns(diagonal, equations.find_max_in_row(diagonal, diagonal));
      }
      
      std::cout << "Uporzadkowane rownanie:\n" << equations << '\n';

      // At this point, we should have correct matrix, solvable with simple method.
      break;
    }
    case GaussMethod::SELECT_FULL: {
      std::cout << "Rozwiazywanie metoda pelnego wyboru.\n";
      for(unsigned diagonal = 0; diagonal < equations.A.size(); diagonal++) {
        unsigned const column_index = equations.find_max_in_row(diagonal, diagonal);
        unsigned const row_index = equations.find_max_in_column(diagonal, diagonal);

        auto const row_value = equations.A[row_index][diagonal];
        auto const column_value = equations.A[diagonal][column_index];

        if (row_value > column_value) {
          equations.swap_rows(diagonal, row_index);
        } else if (column_value > row_value) {
          equations.swap_columns(diagonal, column_index);
        }
      }

      std::cout << "Uporzadkowane rownanie:\n" << equations << '\n';

      // At this point, we should have correct matrix, solvable with simple method.
      break;
    }
    default: {
      break;
    }
  }

  return solve_equation();
}