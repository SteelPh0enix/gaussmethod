#include <iostream>
#include <string>
#include <algorithm>

#include "csv.hpp"
#include "gauss.hpp"
#include "argv_parser.hpp"

int main(int argc, char** argv) {
    ArgumentParser args(argc, argv);

    auto usage = [&]() {
      return "Niepoprawne argumenty.\n"
             + args.executable() + " [simple|row|column|full] [plik1] [plik2] ... [plikN]\n"
             + "simple - uzyj metody prostej\n"
             + "row - uzyj metody wyboru elementu glownego w wierszu\n"
             + "column - uzyj metody wyboru elementu glownego w kolumnie\n"
             + "full - uzyj metody pelnego wyboru elementu";
    };

    if (args.empty()) {
      std::cout << usage();
      return 1;
    }

  auto method_str = args.get<std::string>(0);
  std::transform(method_str.begin(), method_str.end(), method_str.begin(), ::tolower);
  GaussMethod method;

  if (method_str == "simple") {
    method = GaussMethod::SIMPLE;
  } else if (method_str == "row") {
    method = GaussMethod::SELECT_ROW;
  } else if (method_str == "column") {
    method = GaussMethod::SELECT_COLUMN;
  } else if (method_str == "full") {
    method = GaussMethod::SELECT_FULL;
  } else {
    std::cout << usage();
    return 2;
  }

  for (unsigned i = 1; i < args.size(); i++) {
    auto data = read_equations_from_csv_file<double>(args.get<std::string>(i));
    std::cout << args.get<std::string>(i) << ":\n";
    std::cout << data << '\n';
    auto solved = solve_equations_with_gauss(data, method);
    
    if (solved.empty()) {
      std::cout << "Nie udalo sie rozwiazac rowniania dana metoda!\n";
    } else {
      std::cout << "Rozwiazanie (zmienne oznaczone po kolei, od 'a'):\n";

      for(unsigned i = 0; i < solved.size(); i++) {
        std::cout << solved[i].first << " = " << solved[i].second << '\n';
      }
    }
  }

  return 0;
}