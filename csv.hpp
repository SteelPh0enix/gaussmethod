#pragma once

#include <istream>
#include <string>
#include <fstream>
#include <stdexcept>

#include "equation.hpp"

template <typename T>
Equations<T> read_equations_from_csv_stream(std::istream& input) {
  // Read matrix size
  unsigned matrix_size{};
  input >> matrix_size;
  input.ignore(matrix_size + 1);

  // Create return equations and set their sizes to prevent unnecessary
  // reallocations
  Equations<T> ret{};
  ret.A.resize(matrix_size);
  ret.matrix_variables.resize(matrix_size);
  
  for (auto& x : ret.A) {
    x.resize(matrix_size);
  }
  ret.B.resize(matrix_size);

  // Read the data from input stream
  for (unsigned y = 0; y < matrix_size; y++) {
    for (unsigned x = 0; x < matrix_size; x++) {
      input >> ret.A[y][x];
      input.ignore(1);
    }

    input.ignore(1);
    input >> ret.B[y];
    ret.matrix_variables[y] = static_cast<char>('a' + y);
  }

  return ret;
}

template <typename T>
Equations<T> read_equations_from_csv_file(std::string const& filename) {
  std::fstream csv_file(filename);
  if (!csv_file.is_open()) {
    throw std::runtime_error("Nie mozna otworzyc pliku " + filename);
  }
  
  return read_equations_from_csv_stream<T>(csv_file);
}