# Equation solver.

Compiled with `g++ main.cpp -std=c++11 -Wall -Wextra -Wpedantic -o gauss`

## Usage:

`gauss.exe [method] [file1] [file2] ... [fileN]`

where method can be one of following: `full`, `simple`, `row`, `column`.

For example
`gauss.exe full test.csv simple_test.csv`