#pragma once

#include <algorithm>
#include <stdexcept>
#include <string>
#include <typeinfo>
#include <vector>

class ArgumentParser {
 public:
  ArgumentParser(int argc, char** argv) {
    parse_path(std::string(argv[0]));

    for (int i{1}; i < argc; i++) {
      m_argv.push_back(std::string(argv[i]));
    }
  }

  template <typename T>
  T get(std::size_t index) const {
    throw std::invalid_argument(
        std::string("No available conversion to type ") +
        std::string(typeid(T).name()) + std::string("!\n"));
  }

  std::string const& executable() const { return m_executable; }

  std::string const& directory() const { return m_directory; }

  std::string const& path() const { return m_program_path; }

  std::size_t size() const { return m_argv.size(); }

  bool empty() const { return m_argv.size() == 0; }

  std::vector<std::string>::const_iterator begin() const {
    return m_argv.cbegin();
  }

  std::vector<std::string>::const_iterator end() const { return m_argv.cend(); }

  std::vector<std::string>::const_reverse_iterator rbegin() const {
    return m_argv.crbegin();
  }

  std::vector<std::string>::const_reverse_iterator rend() const {
    return m_argv.crend();
  }

 private:
  void parse_path(std::string path) {
    std::replace(path.begin(), path.end(), '\\', '/');
    m_program_path = path;

    auto last_slash_reverse_it = std::find(path.rbegin(), path.rend(), '/');
    auto last_slash_it = std::string::iterator(last_slash_reverse_it.base());

    m_directory = path.substr(0, std::distance(path.begin(), last_slash_it));
    m_executable = path.substr(std::distance(path.begin(), last_slash_it));
  }

  std::vector<std::string> m_argv{};

  std::string m_program_path{};
  std::string m_executable{};
  std::string m_directory{};
};

template <>
std::string ArgumentParser::get<std::string>(std::size_t index) const {
  return m_argv.at(index);
}

template <>
char const* ArgumentParser::get<char const*>(std::size_t index) const {
  return m_argv.at(index).c_str();
}

template <>
int ArgumentParser::get<int>(std::size_t index) const {
  return std::stoi(m_argv.at(index));
}

template <>
long ArgumentParser::get<long>(std::size_t index) const {
  return std::stol(m_argv.at(index));
}

template <>
long long ArgumentParser::get<long long>(std::size_t index) const {
  return std::stoll(m_argv.at(index));
}

template <>
unsigned ArgumentParser::get<unsigned>(std::size_t index) const {
  return static_cast<unsigned>(std::stoul(m_argv.at(index)));
}

template <>
unsigned long ArgumentParser::get<unsigned long>(std::size_t index) const {
  return std::stoul(m_argv.at(index));
}

template <>
unsigned long long ArgumentParser::get<unsigned long long>(
    std::size_t index) const {
  return std::stoull(m_argv.at(index));
}

template <>
float ArgumentParser::get<float>(std::size_t index) const {
  return std::stof(m_argv.at(index));
}

template <>
double ArgumentParser::get<double>(std::size_t index) const {
  return std::stod(m_argv.at(index));
}

template <>
long double ArgumentParser::get<long double>(std::size_t index) const {
  return std::stold(m_argv.at(index));
}
