#pragma once
#include <vector>
#include <ostream>
#include <algorithm>
#include <utility>
#include <limits>
#include <iomanip>

// Recommended usage:
// Matrix<T> m{};
// m[y][x] = value;
template <typename T>
using Matrix = std::vector<std::vector<T>>;

template <typename T>
struct Equations {
  Matrix<T> A;
  std::vector<char> matrix_variables;
  std::vector<T> B;

  // Swaps two columns of matrix
  void swap_columns(unsigned first, unsigned second) {
    if (first == second) return;
    // Column variable is X, so it's necessary to swap every element manually.
    // Size of the column is the size of outer vector
    const unsigned size = A.size();
    for(unsigned i = 0; i < size; i++) {
      std::swap(A[i][first], A[i][second]);
    }

    // Also, swap matrix variables accordingly
    std::swap(matrix_variables[first], matrix_variables[second]);
  }

  // Swaps two rows of matrix and the vector values accordingly
  void swap_rows(unsigned first, unsigned second) {
    if (first == second) return;
    // Row variable is Y, and since Y's are outer vectors, we can just swap them
    std::swap(A[first], A[second]);
    // While swapping rows it's also necessary to swap vector values so the equation
    // system stays correct
    std::swap(B[first], B[second]);
  }

  // Finds max value in matrix column, returns an index to it
  unsigned find_max_in_column(unsigned column, unsigned from = 0) {
    unsigned max_index = from;
    for (unsigned i = from; i < A.size(); i++) {
      if (A[i][column] > A[max_index][column]) {
        max_index = i;
      }
    }

    // std::cout << "Max value in columns " << from << " - " << A.size() << " is A[" << max_index << "][column] = " << A[max_index][column] << '\n';
    return max_index;
  }

  // Finds max value in matrix row, returns an index to it
  unsigned find_max_in_row(unsigned row, unsigned from = 0) {
    unsigned max_index = from;
    for (unsigned i = from; i < A[row].size(); i++) {
      if (A[row][i] > A[row][max_index]) {
        max_index = i;
      }
    }

    // std::cout << "Max value in rows " << from << " - " << A[row].size() << " is A[row][" << max_index << "] = " << A[row][max_index] << '\n';
    return max_index;
  }

  // Substracts `dest` from `source` row values, multiplying by value of `A[dest][x]/A[source][x]` expression.
  // Returns false if A[source][x] == 0.
  bool substract_multiplied_rows(unsigned source, unsigned dest, unsigned x) {
    if (A[source][x] == 0) { return false; }

    const auto multiplier = A[dest][x] / A[source][x];
    for(unsigned elem_x = 0; elem_x < A[dest].size(); elem_x++) {
      A[dest][elem_x] -= A[source][elem_x] * multiplier;
    }

    B[dest] -= B[source] * multiplier;

    return true;
  }
};

template <typename T>
std::ostream& operator<<(std::ostream& os, Equations<T> const& equat) {
  auto backup_flags = os.flags();
  os << std::fixed << std::showpoint << std::setprecision(3);
  
  for(unsigned y = 0; y < equat.B.size(); y++) {
    for(auto const& e: equat.A[y]) {
      os << e << ' ';
    }
    os << "=> " << equat.B[y] << '\n';
  }
  os.flags(backup_flags);
  return os;
}

template <typename T>
std::ostream& operator<<(std::ostream& os, Matrix<T> const& matrix) {
  for (auto const& row : matrix) {
    for (auto const& elem : row) {
      os << elem << ' ';
    }
    os << '\n';
  }

  return os;
}

